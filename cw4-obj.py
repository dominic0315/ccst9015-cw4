#
# Name: cw4.py
#
# Author: C.-H. Dominic HUNG <dominic@teli.hku.hk / chdhung@hku.hk>
#  Technology-Enriched Learning Initiative,
#  The University of Hong Kong
#
# Description: Demonstration program for classwork 4 of CCST9015 offered in
#  Spring 2019. The program showcasts realtime identification of objects in
#  image stream by Google Vision API and highlight distinguished objects and
#  relevant descriptions in a video stream.
#

import image, gvision
import time, signal

# __exit
# ====================
#
# Perform clean up tasks upon receiving termination signals and exit gracefully.
#
# @param signum  Signal dispatched to current process and passed to this handler
# @param curstackCurrent stack frame (Not usually used)
#
def __exit(signum, curstack) :
    if __senhat is not None :
        __senhat.clear()

    quit()

signal.signal(signal.SIGINT, __exit)

#
# USER LOGIC
#

# Declare and define a variable here so when the variable is read later in the
#  program, there is a definition.
#
# Analogy, what can you do if your parents asks your boyfriend or girlfriend
#  to come for family reunion dinner when you have none? (Forever alone!) When
#  you can not handle the case, why would you think your computer knows how to
#  handle non-existing objects? (wink)
#
SAMPLE_SEP = 20
detect_sel = 0

ret = []

agg = 0
ent = time.time()

# A loop that runs forever without stopping
#
while True :
	# Capture a video frame by calling `record_video()' in `image' module.
	#
    frm = image.record_video()

    # If the counter `detect_sel' is divisible by 20, i.e., 0, 20, 40, ...,
    #  call `gvision()' in the `gvision' module and gives the video frame as an
    #  argument for `object' identification.
    #
    # As the submission of a video frame over the WiFi, through the Internet to
    #  distant Google Vision API incurs heavy latency, aka., delay, the choice
    #  of sending 1 video frame in every 20 frames captured is an engineering
    #  compromise for perceived smoother user experience over actual smoothness
    #  in captioning following identifiable objects.
    #
    # !!!Engineering is an art of compromise between theoretical science and
    #  realistics constraints!!!
    #
    # The result returned from the `gvision' function is a long list of objects
    #  distinguishable by the Google Vision with their locations and recognised
    #  object type descriptions.
    #
    # Recall above, if we have not give meaning to `detect_sel', how possible
    #  the computer can divide non-existence with 20 and know the results? It is
    #  non-existence! Not ZERO!
    #
    if detect_sel % SAMPLE_SEP == 0 :
        ret = gvision.gvision(frm, "object")

    # The original captured video frame with the Google Vision result list is
    #  put as argument to `highlight_image' function in `image' module for
    #  image rendering, i.e., framing identified objects and caption the type
    #  an object is identified as. The result from `highlight_image' is directly
    #  feed as an argument to `replay_video' function in `image' module to
    #  trigger a display window or update an existing triggered display window.
    #
    # Note that, if `gvision' is not run in current step, e.g., in counter 1, 2,
    #  ..., 19 and 21, 22, ..., 39 and ..., the stagnant result is used to frame
    #  and tag objects. Therefore, if the up-to-date image shifted from previous
    #  frame too much, the framing will mis-align for sure. But it is again,
    #  a compromise for tolerable user experience.
    #
    image.replay_video(image.highlight_image(frm, ret, txttag = "name"))

    # We advance the counter by 1, this counter will overflow if the program is
    #  executed for infinite time. But for sure, this will not happen, you have
    #  to return this Raspberry Pi to us! =]
    #
    # What is overflow? Imagine we have an A4 paper, write mathematical number
    #  Pi on it.  3.14? The paper can hold 4 characters for sure! What about...
    #  3.141592654 a result from calculator, just 11 characters, fine!
    #
    # But what about I want accurate Pi! And you have to infinitely expand, more
    #  and more digits coming in. The dimension of A4 paper is an example of
    #  physical constraint and there is limit to how many things be written to
    #  an A4. Pi is a real number that can be infinitely expanded depending on
    #  the accuracy we want.
    #
    # Computer space for holding a number is finite, therefore, we we keep
    #  incrementing a number, we will hit a point the computer can no longer
    #  keep all digits and the computer crashes.
    #
    detect_sel += 1

    end = time.time()
    dlt = end - ent
    agg += dlt

    print("Frame Rate: " + str(1 / dlt) + " FPS (Average: " + str(1 / (agg / detect_sel)) + " FPS)")

    ent = time.time()

# Destroy opened video windows.
#
cv2.destroyAllWindows()

#
# (END OF) USER LOGIC
#